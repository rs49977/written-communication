# Caching
### contents :
* __[Definition](#1, "what is caching")__
* __[Working](#2, "How caching works")__
* __[Benefit](#3, "Benefits of caching")__
* __[Usage](#4, "Areas where caching used")__
* __[Platform](#5, "Platforms which uses caching")__


### <a id = 1></a> Definition :
#### What is caching ?
**Caching** is a process to store and access data from cache. 
#### What is Cache ?
[Cache](https://en.wikipedia.org/wiki/Cache_(computing)) is a hardware or software component that stored data, so that future request of data served faster.

### <a id = 2></a>Working :
#### How caching Work ?
The data in a cache is generally stored in fast access hardware such as [RAM](https://en.wikipedia.org/wiki/Random-access_memory) and may also be used with a software component. A cache purpose is to increase data retrieval performance by reducing the access of the slower storage layer.

### <a id = 3></a>Benefit :
#### What are benefits of caching ?
#### Improve app performance 
Because **cache** is stored in fastest [memory](https://en.wikipedia.org/wiki/Computer_memory) devices, reading data from it extremely fast. The result of this it's improves the overall performance of the application.
#### Reduce load on the backend 
By loading some part of [data](https://en.wikipedia.org/wiki/Data) from the backend database to computer memory, caching can reduce the load on database, and avoids slower performance on load.
#### Decreased network costs
Data can be cached at various points in the [network](https://en.wikipedia.org/wiki/Computer_network) path, between the user and origin database. When the data is cached near to the user, requests of the data will not require much additional activity.

### <a id = 4></a>Usage :
#### Usages of caching
* Database
* Web
* Application

### <a id = 5></a>Platforms :
#### Industries where caching used
* Mobile
* IOT 
* Media 
* Social Media 
* Gaming
